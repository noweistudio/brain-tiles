export interface Stats {
    campaign: {
        easy: StatsCampaignGroup,
        normal: StatsCampaignGroup,
        difficult: StatsCampaignGroup,
        tutorial: StatsCampaignGroup
    };
}

export interface StatsCampaignGroup {
    highLevel: number;
    highScore: number;
}
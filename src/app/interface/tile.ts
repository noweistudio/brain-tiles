export interface Tile {
    icon: string;
    color: string;
}
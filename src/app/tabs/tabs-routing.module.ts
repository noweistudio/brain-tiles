import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'tab-games',
                loadChildren: () =>
                    import('../tab-games/tab-games.module').then(m => m.TabGamesPageModule)
            },
            {
                path: 'tab-stats',
                loadChildren: () =>
                    import('../tab-stats/tab-stats.module').then(m => m.TabStatsPageModule)
            },
            {
                path: '**',
                redirectTo: '/tabs/tab-games',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '**',
        redirectTo: '/tabs/tab-games',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}

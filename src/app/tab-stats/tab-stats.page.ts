import {Component} from '@angular/core';
import {Stats} from '../interface/stats';
import {StatsService} from '../service/stats.service';
import {Difficulty} from '../enum/difficulty.enum';

@Component({
    selector: 'app-tab-stats',
    templateUrl: './tab-stats.page.html',
    styleUrls: ['./tab-stats.page.scss'],
})
export class TabStatsPage {
    get stats(): Stats {
        return this.statsService.stats;
    }

    readonly Difficulty = Difficulty;

    constructor(protected statsService: StatsService) {
    }
}

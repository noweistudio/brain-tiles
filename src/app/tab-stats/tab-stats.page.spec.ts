import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabStatsPage } from './tab-stats.page';

describe('TabStatsPage', () => {
  let component: TabStatsPage;
  let fixture: ComponentFixture<TabStatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabStatsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabStatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

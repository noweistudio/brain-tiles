import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {CampaignPageRoutingModule} from './campaign-routing.module';
import {CampaignPage} from './campaign.page';
import {TileOptionsComponent} from '../../component/tile-options/tile-options.component';
import {AdvanceComponent} from '../../component/advance/advance.component';
import {StarsComponent} from '../../component/stars/stars.component';
import {TileComponent} from '../../component/tile/tile.component';
import {RestartComponent} from '../../component/restart/restart.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CampaignPageRoutingModule
    ],
    declarations: [CampaignPage,
        AdvanceComponent,
        RestartComponent,
        StarsComponent,
        TileComponent,
        TileOptionsComponent
    ]
})
export class CampaignPageModule {
}

import { Component } from '@angular/core';
import { StatsService } from '../../service/stats.service';
import { AudioService } from '../../service/audio.service';
import { Tile } from '../../interface/tile';
import { Stats, StatsCampaignGroup } from '../../interface/stats';
import { GameStatus } from '../../enum/game-status.enum';
import { TileService } from '../../service/tile.service';
import { Difficulty } from '../../enum/difficulty.enum';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'app-campaign',
    templateUrl: './campaign.page.html',
    styleUrls: ['./campaign.page.scss'],
})
export class CampaignPage {
    level: number;
    lives = 3;
    score = 0;
    status: GameStatus = GameStatus.Playing;
    difficulty: Difficulty = Difficulty.Easy;
    targets: Array<Tile>;
    targetIndex: number;
    optionIndex: number;

    get stats(): Stats {
        return this.statsService.stats;
    }

    get statsGroup(): StatsCampaignGroup {
        return this.stats.campaign[this.difficulty];
    }

    get timeout(): number {
        switch (this.difficulty) {
            case Difficulty.Easy:
                return 2000;
            case Difficulty.Normal:
                return 1000;
            case Difficulty.Difficult:
                return 500;
            default:
                return 3000;
        }
    }

    constructor(
        protected route: ActivatedRoute,
        protected alertController: AlertController,
        protected audioService: AudioService,
        protected statsService: StatsService) {
    }

    ionViewWillEnter(): void {
        this.statsService.read();

        this.route.queryParams
            .pipe(first())
            .subscribe((params: { difficulty: Difficulty }) => {
                this.difficulty = params.difficulty || Difficulty.Easy;

                if (this.difficulty === Difficulty.Tutorial) {
                    this.presentAlertTutorial();
                } else {
                    this.start();
                }
            });
    }

    async presentAlertTutorial() {
        const alert = await this.alertController.create({
            header: 'How to Play',
            message: '<ol><li class="ion-text-left">Memorize the sequence of tiles.</li><li class="ion-text-left">Select the correct tiles in the sequence shown.</li></ol><p>Hint: <strong>Pulse</strong></p>',
            buttons: [
                {
                    text: 'Got It!',
                    handler: () => {
                        this.start();
                    }
                }
            ]
        });

        await alert.present();
    }

    answer(value: boolean): void {
        if (value) {
            this.audioService.correct()
                .then(() => {
                    this.optionIndex++;

                    if (this.optionIndex >= this.targets.length) {
                        this.status = GameStatus.Advancing;
                        this.score += this.level * this.lives;
                        this.updateStats();
                    }
                });
        } else {
            this.audioService.incorrect()
                .then(() => {
                    this.lives--;
                    this.status = GameStatus.Restarting;
                });
        }
    }

    advance(): void {
        this.lives = 3;
        this.status = GameStatus.Playing;
        this.level++;
        this.start(this.level);
    }

    restart(): void {
        this.status = GameStatus.Playing;

        if (this.lives) {
            this.start(this.level);
        } else {
            this.start();
            this.lives = 3;
            this.score = 0;
        }
    }

    updateStats(): void {
        this.statsService.updateCampaign(
            this.difficulty, {
            ...this.statsGroup,
            highLevel: this.level > this.statsGroup.highLevel ? this.level : this.statsGroup.highLevel,
            highScore: this.score > this.statsGroup.highScore ? this.score : this.statsGroup.highScore
        });
    }

    protected start(level = 1): void {
        this.level = level;
        this.targetIndex = 0;
        this.optionIndex = 0;
        this.status = GameStatus.Playing;
        this.generateTargets();
        this.startTargets();
    }

    protected generateTargets(): void {
        this.targets = Array(this.level)
            .fill(undefined)
            .map(() => TileService.generate());
    }

    protected startTargets(): void {
        this.targets.forEach((v: Tile, i: number) => {
            setTimeout(() => {
                this.targetIndex++;
            }, (i + 1) * this.timeout);
        });
    }
}

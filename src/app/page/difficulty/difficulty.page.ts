import {Component} from '@angular/core';
import {Difficulty} from '../../enum/difficulty.enum';

@Component({
  selector: 'app-difficulty',
  templateUrl: './difficulty.page.html',
  styleUrls: ['./difficulty.page.scss'],
})
export class DifficultyPage {
    readonly Difficulty = Difficulty;
}

export enum GameStatus {
    Playing = 'playing',
    Advancing = 'advancing',
    Restarting = 'restarting'
}
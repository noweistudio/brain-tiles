export enum Difficulty {
    Tutorial = 'tutorial',
    Easy = 'easy',
    Normal = 'normal',
    Difficult = 'difficult'
}

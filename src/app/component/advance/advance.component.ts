import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Difficulty} from '../../enum/difficulty.enum';

@Component({
    selector: 'app-advance',
    templateUrl: './advance.component.html',
    styleUrls: ['./advance.component.scss'],
})
export class AdvanceComponent {
    @Input() level: number;
    @Input() lives: number;
    @Input() score: number;
    @Input() difficulty: Difficulty;
    @Output() nextEmitter: EventEmitter<void> = new EventEmitter<void>();
}

import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'app-restart',
    templateUrl: './restart.component.html',
    styleUrls: ['./restart.component.scss'],
})
export class RestartComponent {
    @Input() level: number;
    @Input() lives: number;
    @Input() score: number;
    @Input() highScore: number;
    @Output() restartEmitter: EventEmitter<void> = new EventEmitter<void>();
}

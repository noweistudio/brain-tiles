import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Tile} from '../../interface/tile';

@Component({
    selector: 'app-tile',
    templateUrl: './tile.component.html',
    styleUrls: ['./tile.component.scss'],
})
export class TileComponent implements OnChanges {
    @Input() tile: Tile;

    show = false;

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.tile) {
            this.show = false;

            setTimeout(() => {
                this.show = true;
            }, 1);
        }
    }
}

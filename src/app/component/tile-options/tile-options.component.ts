import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Difficulty } from '../../enum/difficulty.enum';
import { Tile } from '../../interface/tile';
import { TileService } from '../../service/tile.service';

@Component({
    selector: 'app-tile-options',
    templateUrl: './tile-options.component.html',
    styleUrls: ['./tile-options.component.scss'],
})
export class TileOptionsComponent implements OnChanges {
    @Input() target: Tile;
    @Input() unavailable: Array<Tile>;
    @Input() difficulty: Difficulty;
    @Output() answerEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

    options: Array<Tile>;
    show = false;
    showing = true;
    timeouts: Array<any> = [];

    get hint(): boolean {
        return this.difficulty === Difficulty.Tutorial;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.target) {
            this.timeouts.forEach((v: any) => {
                clearTimeout(v);
            });
            this.show = false;
            this.showing = true;

            this.timeouts.push(setTimeout(() => {
                this.generateOptions();
                this.show = true;
            }, 1));

            this.timeouts.push(setTimeout(() => {
                this.showing = false;
            }, 1500));
        }
    }

    answer(value: Tile): void {
        this.answerEmitter.emit(this.isCorrectAnswer(value));
    }

    isCorrectAnswer(value: Tile): boolean {
        return value.color === this.target.color && value.icon === this.target.icon;
    }

    protected generateOptions(): void {
        const unavailable: Array<Tile> = [this.target].concat(this.unavailable);

        this.options = Array(4)
            .fill(undefined)
            .map(() => {
                const option: Tile = this.generateOptionsHelper(unavailable);
                unavailable.push(option);

                return option;
            });

        this.options[Math.floor(Math.random() * 4)] = this.target;
    }

    protected generateOptionsHelper(unavailable: Array<Tile>): Tile {
        let value: Tile = TileService.generate();

        while (unavailable.find((v: Tile) => v && v.color === value.color && v.icon === value.icon)) {
            value = TileService.generate();
        }

        return value;
    }
}

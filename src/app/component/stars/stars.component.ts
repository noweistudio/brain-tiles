import {Component, Input} from '@angular/core';
import {Difficulty} from '../../enum/difficulty.enum';

@Component({
    selector: 'app-stars',
    templateUrl: './stars.component.html',
    styleUrls: ['./stars.component.scss'],
})
export class StarsComponent {
    @Input() counts: number;
    @Input() difficulty: Difficulty = Difficulty.Easy;
}

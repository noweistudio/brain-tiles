import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {TabGamesPageRoutingModule} from './tab-games-routing.module';
import {TabGamesPage} from './tab-games.page';
import {TileOptionsComponent} from '../component/tile-options/tile-options.component';
import {AdvanceComponent} from '../component/advance/advance.component';
import {TileComponent} from '../component/tile/tile.component';
import {RestartComponent} from '../component/restart/restart.component';
import {StarsComponent} from '../component/stars/stars.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TabGamesPageRoutingModule
    ],
    declarations: [
        TabGamesPage
    ]
})
export class TabGamesPageModule {
}

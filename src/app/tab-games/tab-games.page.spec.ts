import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabGamesPage } from './tab-games.page';

describe('TabGamesPage', () => {
  let component: TabGamesPage;
  let fixture: ComponentFixture<TabGamesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabGamesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabGamesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

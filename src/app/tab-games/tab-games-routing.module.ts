import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: 'campaign',
        loadChildren: () => import('../page/campaign/campaign.module').then( m => m.CampaignPageModule)
    },
    {
        path: 'difficulty',
        loadChildren: () => import('../page/difficulty/difficulty.module').then(m => m.DifficultyPageModule)
    },
    {
        path: '',
        loadChildren: () => import('../page/difficulty/difficulty.module').then(m => m.DifficultyPageModule)
    },
    /*{
        path: '**',
        redirectTo: 'difficulty',
        pathMatch: 'prefix'
    },*/
/*
    {
        path: '',
        component: TabGamesPage
    }
*/
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TabGamesPageRoutingModule {
}

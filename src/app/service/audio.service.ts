import {Injectable} from '@angular/core';

const AUDIO_CORRECT = 'correct.mp3';
const AUDIO_INCORRECT = 'incorrect.mp3';

@Injectable({
    providedIn: 'root'
})
export class AudioService {
    correct(): Promise<void> {
        return this.playAudio(AUDIO_CORRECT);
    }

    incorrect(): Promise<void> {
        return this.playAudio(AUDIO_INCORRECT);
    }

    protected playAudio(src: string): Promise<void> {
        const audio = new Audio();
        audio.src = `../../../assets/audio/${src}`;
        audio.load();

        return audio.play();
    }
}

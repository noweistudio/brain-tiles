import {Injectable} from '@angular/core';
import {Stats, StatsCampaignGroup} from '../interface/stats';
import {Storage} from '@ionic/storage';
import {Difficulty} from '../enum/difficulty.enum';

const EMPTY_STATS_CAMPAIGN_GROUP: StatsCampaignGroup = {
    highLevel: 0,
    highScore: 0
};
const EMPTY_STATS: Stats = {
    campaign: {
        easy: EMPTY_STATS_CAMPAIGN_GROUP,
        normal: EMPTY_STATS_CAMPAIGN_GROUP,
        difficult: EMPTY_STATS_CAMPAIGN_GROUP,
        tutorial: EMPTY_STATS_CAMPAIGN_GROUP
    }
};

@Injectable({
    providedIn: 'root'
})
export class StatsService {
    stats: Stats;

    constructor(protected storage: Storage) {
        this.read();
    }

    create(stats: Stats = EMPTY_STATS): void {
        this.update(stats);
    }

    read(): void {
        this.storage.get('stats')
            .then((v: Stats) => {
                this.stats = Object.assign(EMPTY_STATS, (v || {}));
            });
    }

    update(stats: Stats = this.stats): void {
        this.stats = stats;
        this.storage.set('stats', stats)
            .then();
    }

    updateCampaign(difficulty: Difficulty, value: StatsCampaignGroup): void {
        this.stats.campaign[difficulty] = value;
        this.update();
    }
}

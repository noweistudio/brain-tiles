import {Injectable} from '@angular/core';
import {Tile} from '../interface/tile';

const COLORS = ['primary', 'secondary', 'tertiary', 'success', 'warning', 'danger', 'medium', 'dark'];
const ICONS = ['sunny', 'moon', 'star', 'heart'];

@Injectable({
    providedIn: 'root'
})
export class TileService {
    static generate(): Tile {
        return {
            icon: ICONS[Math.floor(Math.random() * ICONS.length)],
            color: COLORS[Math.floor(Math.random() * COLORS.length)]
        };
    }
}

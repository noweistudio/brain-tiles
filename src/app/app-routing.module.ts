import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
    },
    {
        path: 'tab-stats',
        loadChildren: () => import('./tab-stats/tab-stats.module').then(m => m.TabStatsPageModule)
    },
    {
        path: 'tab-games',
        loadChildren: () => import('./tab-games/tab-games.module').then(m => m.TabGamesPageModule)
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
